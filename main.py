import os
import json
from flask import Flask


app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Welcome Jenish Lila! You are authenticated to use the API.</p>"

@app.route("/send-data")
def send_data():
    username = os.environ.get("Username", "mongodb_test_user")
    password = os.environ.get("Password", "mongodb_test_password")
    cluster_url = os.environ.get("Cluster URL", "mongodb://mongodb_test_user:mongodb_test_password@127.0.0.1:27017/")
    x = '{"cluster_url":'+cluster_url+'", Username":'+username+', "Password":'+password+', "DATA:{"age": 50,"height": 182, "weight": 85}}'
    return x